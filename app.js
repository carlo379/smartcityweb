// Basic setup for NodeJS server

/* Libraries and Packages - Dependencies
 =======================================*/
var express = require('express')                // Node Framework
    , bodyParser = require('body-parser')       // Parse body of REST Requests
    , app = express()                           // Define app variable
    , port = process.env.PORT || 8080           // Define port the app will be using
    , http = require('http')		            // Require http server
    , path = require('path')                    // Handling of file paths
    , gets = require('./routes/gets')           // Define routes path
    , posts = require('./routes/posts')         // Define routes path
    , async = require('async')                  // Perform Asynchronous functions
    , serveStatic = require('serve-static')     // Serve Static Files
    , cookieParser = require('cookie-parser')   // Enable Cookies in Express app
    , logger = require('morgan');               // Enable Logger

/* VIEW ENGINE - EJS
 set the view engine to ejs - View Templates to integrate Javascript with HTML
 =============================================================================*/
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');

/* Midlewares
 ================*/

app.use(logger('dev'));                                             // Log in Development environment
app.use(bodyParser.urlencoded({ extended: true, limit: '6mb' }));   // configure "app" to use bodyParser() to handle date from POST
app.use(bodyParser.json());                                         // define parse format - JSON
app.use('/public',serveStatic(__dirname + '/public/'));             // Serve Static Files
app.use(cookieParser());                                            // Enable Cookies on App

/* ROUTES  -  GET
 ================*/
for(var get in gets) {
    app.get(gets[get].path, gets[get].fn);
}

/* ROUTES - POSTS
 ================*/
for(var post in posts) {
    app.post(posts[post].path, posts[post].fn);
}

/* Catch 404 and forward to error handler
=========================================*/
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/* ERROR HANDLERS-DEVELOPMENT *
 * will print stacktrace      *
==============================*/
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

/* ERROR HANDLERS-PRODUCTION   *
 * will NOT print stacktrace   *
 ==============================*/
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

/* Start Server
 ================*/
async.parallel([
    function () {
        // Begin listening for HTTP requests to Express app
        http.createServer(app).listen(port, function () {
            console.log("Listening on " + port);
        });
    }
]);


