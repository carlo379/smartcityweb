// posts.js
// Calls for Libraries to be used
var _ = require('underscore')   // Javascript Helper Library
    , path = require('path');   // Handling of file paths

/* POST routes
 ======================*/
var comment_fn = function (req, res, next) {
    // Render Something
};

 /* Map Routes
 ======================*/
var define_routes = function(dict) {
    var toroute = function(item) {
        return _.object(_.zip(['path', 'fn'], [item[0], item[1]]));
    };
    return _.map(_.pairs(dict), toroute);
};

/* Define Routes
 ======================*/
var routes = define_routes({
    '/comment': comment_fn
});

module.exports = routes;