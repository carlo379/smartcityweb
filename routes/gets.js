// gets.js
// Calls for Libraries to be used
var _ = require('underscore')   // Javascript Helper Library
    , path = require('path');   // Handling of file paths

/* GET routes
 ======================*/
var index_fn = function(req, res, next) {
    res.render("index");
};

var smartstreet_fn = function(req, res, next) {
    res.render("smartstreet");
};

/* Map Routes
 ======================*/
var define_routes = function(dict) {
    var toroute = function(item) {
        return _.object(_.zip(['path', 'fn'], [item[0], item[1]]));
    };
    return _.map(_.pairs(dict), toroute);
};

/* Define Routes
 ======================*/
var routes = define_routes({
    '/': index_fn,
    '/smartstreet': smartstreet_fn
});

module.exports = routes;